# sshot
SSHot! use ssh to upload a screenshot to your server! 

## Requirements
Requires *xsel*, *scrot* and *ssh*. 

I recommend giving your server your ssh key's fingerprint so you do not have to enter your password constantly.
